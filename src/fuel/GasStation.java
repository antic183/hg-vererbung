package fuel;

import flight.Flight;

import java.util.List;

public class GasStation {

    public void refuel(List<Flight> airplaineList) {
        for(Flight flight: airplaineList) {
            flight.tankUp();
        }
    }
}
