package client;

import flight.CargoFlight;
import flight.Flight;
import fuel.GasStation;
import pack.Pack;
import pack.Passenger;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        // UI wurde nicht implementiert ..
        /*UI ui = new UI();
        ui.showMenue();*/

        List<Flight> airplaineList = new ArrayList<>();
        airplaineList.add(new CargoFlight("1234-12", "Jumbo"));
        airplaineList.add(new Flight("321-31", "Airbus"));
        airplaineList.add(new CargoFlight("145-12", "Airbus"));

        //tanken:
        GasStation gasStation = new GasStation();
        gasStation.refuel(airplaineList);

        //__ausgabe des Tankinhalts nach dem tanken:
        for (Flight flight: airplaineList) {

        }

        //beladen:
        for (Flight flight: airplaineList) {
            flight.load();
        }

        //Ausgabe der Flugzeugtypen und deren Ladung
        for (Flight flight: airplaineList) {
            Map<String, String> type = new HashMap<>();

            type.put("flugzeugTyp" , flight instanceof CargoFlight? "CargoFlugzeug": "Passagierflugzeug");
            type.put("fracht", flight instanceof CargoFlight? "Packeten": "Personen");

            String trenner = "";
            for(int i=0; i<100; i++) {
                trenner+="-";
            }

            System.out.println(trenner);

            System.out.printf(
                    "das %s mit der Flugnummer \"%s\", vom Typ \"%s\", hat einen aktuellen Tankinhalt von %d Liter Kerosin.%n",
                    type.get("flugzeugTyp"),
                    flight.getFlightNumber(),
                    flight.getType(),
                    flight.getTankCapacity()
            );
            List<Pack> packages = flight.getPackage();
            System.out.printf("Transportiert: %s, ist momentan mit %s, %s beladen %nDetails: %s%n%n",
                    type.get("fracht"), packages.size(), type.get("fracht"), packages
            );
        }

    }
}
