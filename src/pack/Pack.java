package pack;

import java.util.GregorianCalendar;

public class Pack {
    protected long id;

    public Pack() {
        id = new GregorianCalendar().getTimeInMillis();
    }

    @Override
    public String toString() {
        return "Cargo: id = " + id + ";";
    }
}
