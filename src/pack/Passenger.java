package pack;


import java.util.GregorianCalendar;

public class Passenger extends Pack {
    String firstName;
    String lastName;
    int age;

    public Passenger(String firstName, String lastName, int age) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Passanger: firstname = " + firstName + ", lastname = " + lastName + ", age = " + age + ", id = " + id + ";";
    }
}
