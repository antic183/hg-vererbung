package flight;

import pack.Pack;
import pack.Passenger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CargoFlight extends Flight {

    protected int maxtankCapacity = 8000;
    List<Pack> packets;

    public CargoFlight(String flightNr, String type) {
        super(flightNr, type);
    }

    @Override
    public void tankUp() {
        tankCapacity = maxtankCapacity;
    }

    @Override
    public void load() {
        packets = new ArrayList<>();
        Random rand = new Random();

        for(int i=0; i<rand.nextInt(50) + 1; i++) {
            packets.add(new Pack());
        }
    }

    @Override
    public List<Pack> getPackage () {
        return packets;
    }
}
