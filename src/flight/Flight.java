package flight;

import pack.Pack;
import pack.Passenger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Flight {
    protected String flightNumber;
    protected String type;

    protected int maxtankCapacity = 5000;
    protected int tankCapacity;

    List<Pack> packets;

    public Flight(String flightNumber, String type) {
        this.flightNumber = flightNumber;
        this.type = type;
    }

    public void tankUp() {
        tankCapacity = maxtankCapacity;
    }

    public int getTankCapacity(){
        return tankCapacity;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getType() {
        return type;
    }

    /**
     * add random passangers
     */
    public void load() {
        packets = new ArrayList<>();

        Random rand = new Random();
        int countPassangers = rand.nextInt(290) + 1;
        char[] alphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
        for(int i=0; i<countPassangers; i++) {
            int randAge = rand.nextInt(115) + 1;
            String randFirstName = "" + alphabet[rand.nextInt(alphabet.length)]
                    + alphabet[rand.nextInt(alphabet.length)]
                    + alphabet[rand.nextInt(alphabet.length)];
            String randLastName = "" + alphabet[rand.nextInt(alphabet.length)]
                    + alphabet[rand.nextInt(alphabet.length)]
                    + alphabet[rand.nextInt(alphabet.length)];

            packets.add(new Passenger(randFirstName, randLastName, randAge));
        }
    }

    public List<Pack> getPackage () {
        return packets;
    }

}
